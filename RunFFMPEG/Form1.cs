﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RunFFMPEG
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = ofd.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                textBox3.Text = ofd.FileName;
            }
            try
            {
                string lines = File.ReadAllText(textBox3.Text);
                textBox2.Text = lines;
            }
            catch(Exception x)
            {
                MessageBox.Show("File not Found"+x.Message);
                return;
            }
           
        }

        private void Render_Click(object sender, EventArgs e)
        {
            Render();
        }
        public void Render()
        {
            string input = textBox1.Text.Trim();
            if (!File.Exists(input))
            {
                MessageBox.Show("Input not found!");
                return;
            }
            string code = textBox2.Text.Trim();
            code = code.Replace("{input}", input);
            code += @" output.mp4";
            System.Diagnostics.Process.Start("CMD.exe", "/k "+code);

        }

        private void Preview_button_Click(object sender, EventArgs e)
        {
            string input = textBox1.Text.Trim();
            if (!File.Exists(input))
            {
                MessageBox.Show("Input not found!");
                return;
            }
          string code = textBox2.Text.Trim();
          code = code.Replace("{input}", input);
        code += " -f matroska - | ffplay -";
            System.Diagnostics.Process.Start("CMD.exe", "/k "+code);
            
        }
    }
}
